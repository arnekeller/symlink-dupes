use std::ffi::OsString;
use std::{collections::HashMap, env, fs, hash::Hasher, os::unix::fs::symlink, path::PathBuf};

use ahash::AHasher;

fn main() {
	let args = env::args_os().collect::<Vec<_>>();
	if args.len() != 2 {
		println!("usage: ./symlinkDupes <directory>");
		return;
	}
	let dir: PathBuf = PathBuf::from(&args[1]);
	let mut filenames = fs::read_dir(&dir).unwrap()
		.map(Result::unwrap)
		.map(|x| (x.metadata().unwrap(), x.file_name()))
		.filter(|(metadata, _)| metadata.is_file()) // exclude symlinks and directories
		.map(|(metadata, name)| (metadata.modified().unwrap(), name))
		.collect::<Vec<_>>();
	filenames.sort();
	let mut hashes = HashMap::new();
	let hasher = AHasher::new_with_keys(1234340981, 567840211);
	for file in &filenames {
		let full_path = dir.join(&file.1);
		let content = fs::read(&full_path).unwrap();
		if content.len() > 0 {
			let mut hasher = hasher.clone();
			hasher.write(&content);
			let hash = hasher.finish();
			let key = (hash, content.len());
			if hashes.contains_key(&key) {
				fs::remove_file(&full_path).unwrap();
				symlink(hashes[&key], &full_path).unwrap();
				println!("linking {} -> {}", full_path.display(), (&hashes[&key] as &OsString).to_string_lossy());
				fs_set_times::set_symlink_times(&full_path, None, Some(file.0.into())).unwrap();
			} else {
				hashes.insert(key, &file.1);
			}
		}
	}
}
